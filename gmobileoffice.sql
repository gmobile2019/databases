-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: gmobileoffice
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_updates`
--

DROP TABLE IF EXISTS `activity_updates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `projections` text COLLATE utf8_unicode_ci,
  `comments` text COLLATE utf8_unicode_ci,
  `activities` text COLLATE utf8_unicode_ci,
  `pendings` text COLLATE utf8_unicode_ci,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `tasktitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `completiondate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_updates`
--

LOCK TABLES `activity_updates` WRITE;
/*!40000 ALTER TABLE `activity_updates` DISABLE KEYS */;
INSERT INTO `activity_updates` VALUES (2,'DSE SMS Alerts','Incomplete','this will be completed by the end of the week','n/a','Get in contact with someone from Vodacom and book a meeting','Feedback from Vodacom to confirm the meeting','2017-02-08 17:01:17',10,NULL,NULL,NULL,NULL),(3,'LETSHEGO ','Incomplete','n/a','n/a','Test Cards will be send on Tuesday','n/a','2017-02-10 15:57:30',9,NULL,NULL,NULL,NULL),(4,'Purchase cell phones for the office','Complete','n/a','n/a','Purchase mobile phones for the company','n/a','2017-02-10 16:41:38',10,NULL,NULL,NULL,NULL),(5,'Twitter For G-Mobile','Incomplete','mid-week','Limitations on words by Twitter(160characters)','Create a twiiter account and link it to the official site','Content to be kept on the twitter bio','2017-02-10 16:45:03',10,NULL,NULL,NULL,NULL),(6,'Twitter For G-Mobile','Incomplete','mid-week','Limitations on words by Twitter(160characters)','Create a twiiter account and link it to the official site','Content to be kept on the twitter bio','2017-02-10 16:45:11',10,NULL,NULL,NULL,NULL),(7,'MCB Alerts and Notifications','Complete','n/a','n/a','follow up on sms delivery and sender id','n/a','2017-02-17 16:14:44',1,NULL,NULL,NULL,NULL),(8,'Twitter For G-Mobile','Complete','n/a','n/a','Upload press releases on the website','n/a','2017-02-24 16:05:03',10,NULL,NULL,NULL,NULL),(9,'Twitter For G-Mobile','Complete','n/a','n/a','Create a twiiter account and link it to the official site','n/a','2017-02-24 16:05:33',10,NULL,NULL,NULL,NULL),(10,'DSE SMS Alerts','Complete','n/a','Voda rejected the proposal','Get in contact with someone from Vodacom and book a meeting','n/a','2017-02-24 16:07:36',10,NULL,NULL,NULL,NULL),(11,'Retrieve Sample Cards from Aramex','Complete','n/a','Found the parcel and has been delivered','Original parcel wasn\'t delivered, had to trace its whereabouts','n/a','2017-02-24 16:08:58',10,NULL,NULL,NULL,NULL),(12,'DSE SMS Alerts','Incomplete','n/a','Waiting for a callback','Book an appointment with Halotel to discuss working together','the head of VAS is on holiday, will be back on Monday and should have feedback by the end of Monday.','2017-02-24 16:13:14',10,NULL,NULL,NULL,NULL),(13,'DSE SMS Alerts','Incomplete','n/a','n/a','Book an appointment with TTCL VAS','Have sent an email requesting an meeting. Still waiting for feedback','2017-02-24 16:14:27',10,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `activity_updates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_book` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PHONE1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PHONE2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ORGANIZATION` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHY_ADDRESS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `CREATEDBY` int(11) NOT NULL,
  `CREATEDON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODIFIEDBY` int(11) NOT NULL,
  `MODIFIEDON` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_book`
--

LOCK TABLES `address_book` WRITE;
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_categories`
--

DROP TABLE IF EXISTS `business_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `CREATEDBY` int(11) NOT NULL,
  `CREATEDON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIEDBY` int(11) NOT NULL,
  `LASTUPDATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_categories`
--

LOCK TABLES `business_categories` WRITE;
/*!40000 ALTER TABLE `business_categories` DISABLE KEYS */;
INSERT INTO `business_categories` VALUES (1,'BANKS',NULL,9,'2016-12-06 10:47:08',0,'0000-00-00 00:00:00'),(2,'OTHERS','ORGANIZATIONS',9,'2016-12-07 09:26:15',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `business_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECTID` int(11) NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PHONE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TITLE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `USERID` int(11) NOT NULL,
  `MODIFIEDBY` int(11) NOT NULL,
  `ACTIONDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LASTUPDATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contract_details`
--

DROP TABLE IF EXISTS `contract_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contract_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECTID` int(11) NOT NULL,
  `TOTALVALUE` double NOT NULL,
  `PERFOMANCEDURATION` int(11) NOT NULL,
  `YEAR_ONE` float NOT NULL,
  `YEAR_TWO` float DEFAULT NULL,
  `YEAR_THREE` float DEFAULT NULL,
  `YEAR_FOUR` float DEFAULT NULL,
  `ACTIONDATE` datetime NOT NULL,
  `LASTUPDATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contract_details`
--

LOCK TABLES `contract_details` WRITE;
/*!40000 ALTER TABLE `contract_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `contract_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `ID` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(100) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'SuperAdministrator','System\'s administrator'),(3,'User','G mobile user');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operational_details`
--

DROP TABLE IF EXISTS `operational_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operational_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECTID` int(11) NOT NULL,
  `RISKS` text COLLATE utf8_unicode_ci,
  `COMPETITION` text COLLATE utf8_unicode_ci,
  `USERID` int(11) NOT NULL,
  `LASTUPDATE` datetime NOT NULL,
  `MODIFIEDBY` int(11) NOT NULL,
  `ACTIONDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operational_details`
--

LOCK TABLES `operational_details` WRITE;
/*!40000 ALTER TABLE `operational_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `operational_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfomance_durations`
--

DROP TABLE IF EXISTS `perfomance_durations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfomance_durations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DURATION` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfomance_durations`
--

LOCK TABLES `perfomance_durations` WRITE;
/*!40000 ALTER TABLE `perfomance_durations` DISABLE KEYS */;
INSERT INTO `perfomance_durations` VALUES (1,'One Year',1),(2,'Two Years',2),(3,'Three Years',3),(4,'Four Years',4),(5,'Five Years',5),(6,'Six Years',6),(7,'Seven Years',7),(8,'Eight Years',8),(9,'Nine Years',9),(10,'Ten Years',10);
/*!40000 ALTER TABLE `perfomance_durations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products_services`
--

DROP TABLE IF EXISTS `products_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products_services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '1',
  `ACTIONDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LASTUPDATE` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products_services`
--

LOCK TABLES `products_services` WRITE;
/*!40000 ALTER TABLE `products_services` DISABLE KEYS */;
INSERT INTO `products_services` VALUES (1,'SMART CARDS SOLUTION','Multi application smart cards, Id cards, Authorisation cards',1,'2016-12-07 20:14:54','0000-00-00 00:00:00'),(2,'CORE BANKING SYSTEM','Support bank\'s most common transactions.',1,'2016-12-07 20:16:10','0000-00-00 00:00:00'),(3,'MOBILE ONBOARDING SOLUTION','Mobile Enrolment of Customers',1,'2016-12-07 20:16:47','0000-00-00 00:00:00'),(4,'MICRO-LENDING SOLUTION','lending small volumes of money to customers',1,'2016-12-07 20:18:04','0000-00-00 00:00:00'),(5,'BANKING CHANNELS','The automated delivery of banking products and services directly to customers',1,'2016-12-07 20:21:11','0000-00-00 00:00:00'),(6,'MOBILE POINT OF SALE SOLUTION','electronic point of sale terminal',1,'2016-12-07 20:24:11','0000-00-00 00:00:00'),(7,'SECURITY STAMPS SOLUTION','Track and Trace security solutions',1,'2016-12-07 20:29:29','0000-00-00 00:00:00'),(8,'BULK SMS','The dissemination of large numbers of SMS messages for delivery to mobile phone terminals. It is used by media companies, enterprises, banks (for marketing and fraud control) and consumer brands for a variety of purposes including entertainment, enterprise and mobile marketing.',1,'2016-12-07 20:38:16','0000-00-00 00:00:00'),(9,'OTHERS','Customized Technological Solutions',1,'2016-12-07 21:15:12','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `products_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_details`
--

DROP TABLE IF EXISTS `project_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CATEGORY` int(11) NOT NULL,
  `ORGANIZATION` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `SALESREP` int(11) NOT NULL,
  `TOTALVALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PERFOMANCEDURATION` int(11) NOT NULL,
  `YEAR1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `YEAR2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YEAR3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YEAR4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `CREATEDBY` int(11) NOT NULL,
  `CREATEDON` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MODIFIEDBY` int(11) NOT NULL,
  `MODIFIEDON` datetime NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_details`
--

LOCK TABLES `project_details` WRITE;
/*!40000 ALTER TABLE `project_details` DISABLE KEYS */;
INSERT INTO `project_details` VALUES (1,'SMART CARDS',2,'TANZANIA REVENUE AUTHORITY','Supply of cards and consumables for the computerised driver\'s License.',9,'1,977,418.50',1,'2017',NULL,NULL,NULL,0,9,'2016-12-07 12:26:43',9,'2016-12-14 16:23:13'),(2,'ELECTRONIC TAX STAMPS',2,'TANZANIA REVENUE AUTHORITY','Supply, Installation and Operation of System for Electronic Tax Stamps under Self Financing Scheme Tender No. AE/023/2015-16/HQ/G/002)',9,'00000',3,'2017',NULL,NULL,NULL,0,9,'2016-12-07 12:35:30',9,'2016-12-14 16:23:32'),(3,'BULK SMS',2,'DAR-ES-SALAAM STOCK EXCHANGE','SMS subscription service to notify clients and other non-investor users of all listed company related activities as they happen at the stock exchange.',9,'$5,004.59',2,'2017',NULL,NULL,NULL,0,9,'2016-12-07 12:46:33',9,'2016-12-15 16:37:24'),(4,'BANKING CARDS',1,'AMANA BANK','Supply of EMV cards',9,'$36,600.00',1,'2017',NULL,NULL,NULL,0,9,'2016-12-07 13:00:48',9,'2016-12-14 16:24:47'),(5,'SMS ALERTS & NOTIFICATION',2,'MWALIMU COMMERCIAL BANK','Provide Alerts and Notifications to customers regarding the ongoing banking activities.',9,'$17,700',2,'2016',NULL,NULL,NULL,1,9,'2016-12-07 13:04:55',9,'2016-12-15 16:01:08'),(6,'INTERNET BANKING SYSTEM',1,'NATIONAL MICRO-FINANCE BANK','Provide a platform for internet banking.',9,'000000',2,'2017',NULL,NULL,NULL,0,9,'2016-12-07 13:07:06',9,'2016-12-15 16:06:07'),(7,'PERSONALIZATION MACHINES',2,'NATIONAL IDENTIFICATION AUTHORITY','Supply, Installation, and Maintenance of additional Personalization machines.',9,'2,828,178.98',2,'2017',NULL,NULL,NULL,0,9,'2016-12-07 13:10:06',9,'2017-02-24 12:54:01'),(8,'WAPI PROJECT',2,'DIGITAL CITY (AM3)','Provide Technical Support and Reconciliation of Transactions.',8,'000000',2,'2017',NULL,NULL,NULL,0,9,'2016-12-07 13:18:12',0,'0000-00-00 00:00:00'),(9,'MICRO-LENDING SOLUTION',2,'DESTINY FINANCIAL','Provide the micro-lending solutions through smart phones.',9,'00000',2,'2017',NULL,NULL,NULL,0,9,'2016-12-07 13:22:41',9,'2016-12-15 16:08:25'),(10,'PERSO CENTER',2,'G-MOBILE','Starting a personalisation Centre in Tanzania (East Africa)',9,'000000',3,'2017',NULL,NULL,NULL,0,9,'2016-12-07 13:24:21',0,'0000-00-00 00:00:00'),(11,'MAGNETIC STRIPE BANK CARDS',1,'LETSHEGO BANK ','Supply of EMV CPA Contact bank cards',9,'$9,735',2,'2017',NULL,NULL,NULL,0,9,'2016-12-15 13:30:53',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `project_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_products_services`
--

DROP TABLE IF EXISTS `project_products_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_products_services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECTID` int(11) NOT NULL,
  `PRODUCTID` int(11) NOT NULL,
  `ACTIONDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_products_services`
--

LOCK TABLES `project_products_services` WRITE;
/*!40000 ALTER TABLE `project_products_services` DISABLE KEYS */;
INSERT INTO `project_products_services` VALUES (13,1,1,'2016-12-14 13:23:13'),(14,2,7,'2016-12-14 13:23:32'),(29,3,8,'2016-12-15 13:37:24'),(19,4,1,'2016-12-14 13:24:47'),(20,5,8,'2016-12-15 13:01:08'),(22,6,5,'2016-12-15 13:06:07'),(31,7,1,'2017-02-24 09:54:01'),(8,8,9,'2016-12-07 13:18:12'),(27,9,4,'2016-12-15 13:08:25'),(10,10,9,'2016-12-07 13:24:21'),(28,11,1,'2016-12-15 13:30:53');
/*!40000 ALTER TABLE `project_products_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales_activity`
--

DROP TABLE IF EXISTS `sales_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales_activity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROJECTID` int(11) NOT NULL,
  `CLOSE` int(11) NOT NULL,
  `STAGE` int(11) NOT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `USERID` int(11) NOT NULL,
  `ACTIONDATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales_activity`
--

LOCK TABLES `sales_activity` WRITE;
/*!40000 ALTER TABLE `sales_activity` DISABLE KEYS */;
INSERT INTO `sales_activity` VALUES (1,1,20,1,'',9,'2016-12-15 13:11:08'),(2,2,20,1,'',9,'2016-12-15 13:11:25'),(3,3,80,3,'Waiting for final Payments of the installations and implementation of SMS Alerts and Notification System.<br />\r\nWaiting for the PO of Bulk SMS',9,'2016-12-15 13:13:38'),(4,4,60,3,'- NDA was signed, waiting to receive script manual and test cards.<br />\r\n- 2nd Payments will be done after delivery of the cards.',9,'2016-12-15 13:16:19'),(5,6,20,1,'- Waiting for the approval meeting that will be done on the 10/01/2016',9,'2016-12-15 13:17:18'),(6,7,20,1,'-Waiting to submit the  tender proposal.',9,'2016-12-15 13:18:13'),(7,9,20,0,'Pending',9,'2016-12-15 13:22:01'),(8,10,40,1,'- Payment was done upon purchase order.<br />\r\n-Waiting for Salim to send the detailed technical specifications document.',9,'2016-12-15 13:25:27'),(9,11,40,1,'-Send an LPO and Card designs<br />\r\n-Send and invoice, Waiting for the 60% down Payments<br />\r\n-',9,'2016-12-15 13:32:29'),(10,1,20,1,'Submitted the Bid validity and Bid bond extension due to the extension of evaluation days. Waiting for TRA\'s Response',9,'2017-02-24 09:04:18'),(11,2,20,1,'On the 10/02/2017 Ashton Potter Submitted the extended validity period for 90 days (8/05/2017). Waiting for Feedback from TRA.',9,'2017-02-24 09:16:35'),(12,3,20,1,'AIRTEL & TIGO ( Waiting for contracts to be approved by the VAS team)<br />\r\nVODACOM & HALOTEL (Requested that the Business case should be revised)<br />\r\nTTCL (Meeting with VAS team on Monday to plan for a presentation.',9,'2017-02-24 09:23:21'),(13,4,40,2,'The cards have been shipped, although they are not yet delivered due to customs.<br />\r\nPersonalization software is being developed. The software is expected to be delivered in Mid March<br />\r\nSample cards are expected to be shipped next week.',9,'2017-02-24 09:29:14'),(14,5,100,4,'Bought a bundle of 50,000 SMS on 24/02/2017',9,'2017-02-24 09:33:21'),(15,6,20,0,'The project has been re-tendered.',9,'2017-02-24 09:35:00'),(16,7,20,1,'Send the tender document. Waiting for NIDA\'s response.',9,'2017-02-24 09:44:25'),(17,10,40,2,'Second Payments were done, Waiting for the schedule for the FAT which is predicted to be in April.',9,'2017-02-24 09:51:41'),(18,11,40,2,'Test cards have been send to Letshego for approval.',9,'2017-02-24 09:52:23');
/*!40000 ALTER TABLE `sales_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IP_ADDRESS` int(10) NOT NULL,
  `USERNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `SALT` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `CREATEDON` varchar(14) NOT NULL,
  `LASTLOGIN` varchar(20) DEFAULT NULL,
  `ACTIVE` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `FIRST_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MIDDLE_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MSISDN` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `MODIFIEDON` varchar(14) DEFAULT NULL,
  `MODIFIEDBY` int(11) DEFAULT NULL,
  `CREATEDBY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,127001,'Manuel','e268c1352c7a62e558a648ce6710a5b4','123456','kemixenock@gmail.com','4294967295','1489581637',1,'Emmanuel','Enock','Mfikwa','+255719186759','20160216110014',1,NULL),(8,0,'anselm','d8ddb5ff066a2aa4046faa486ad976e7','363ce3b793','ansel@yahoo.com','20160219111053','1481803291',1,'Anselm',NULL,'Missango','+255719186759','20161206113830',1,1),(9,19645,'rhoda.mchunga','7887d8fcb26fca2a6c7dc1c99dd9caf3','08c30a949f',NULL,'20161206121410','1487925167',1,'Rhoda',NULL,'Mchunga','255',NULL,NULL,1),(10,19645,'benny123','50a0ca597aa079a0f3fd706364674fcf','99091a2cfd',NULL,'20170208112154','1487941305',1,'Benny',NULL,'Benny','255',NULL,NULL,1),(11,19645,'emmanuel','0514c5ad113d0d1ae0817f75e340eb5d','9e6a6e7c24',NULL,'20170315105138','1489581662',1,'Emmanuel',NULL,'Mfikwa','255719186759',NULL,NULL,1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (1,1,1),(12,11,3),(11,10,3),(10,9,3),(9,8,3);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-18 11:57:16
